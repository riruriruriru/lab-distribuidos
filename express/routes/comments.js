var express = require('express');
var CommentsCtlr = require('../controllers/comments');
var comments = express.Router();
/* GET users listing. */


comments.route('/all').get(CommentsCtlr.findAllComments)
comments.route('/sortByAuthors').get(CommentsCtlr.findByAuthors)
comments.route('/sortBySubreddit').get(CommentsCtlr.findBySubreddit)
comments.route('/countWords').get(CommentsCtlr.wordAggregation)
comments.route('/countStopWords').get(CommentsCtlr.stopWordsAggregation)
comments.route('/sortByLanguage').get(CommentsCtlr.findByLanguage)
comments.route('/sortByPost').get(CommentsCtlr.findByPost)
comments.route('/createsentiment').post(CommentsCtlr.saveSentiment)
comments.route('/status').put(CommentsCtlr.findAllAndUpdate)

comments.route('/sentiment').get(CommentsCtlr.findPosNegNeu)

module.exports = comments;