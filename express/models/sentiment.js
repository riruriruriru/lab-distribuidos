var mongoose = require('mongoose');  
var sentimentSchema = new mongoose.Schema({  
  pos: Number,
  neu: Number,
  neg: Number, 
  totalVowel: Number,
  totalConsonant: Number,
  totalUpper: Number,
  totalLower: Number,
  totalStopWords: Number,
  totalNonStopWords: Number,
  totalWords: Number,
  totalSentences: Number,
  totalUsers: Number,
  totalSubreddits: Number,
  totalPosts: Number,
  totalLanguages: Number,
  total: Number,
});
mongoose.model('sentiment', sentimentSchema);