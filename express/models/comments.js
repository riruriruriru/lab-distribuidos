var mongoose = require('mongoose');  
var reddit_commentsSchema = new mongoose.Schema({  
  comment: {
  	type: String
  },
  author: String,
  score: Number,
  id: Number, 
  idSubmission: Number,
  submission: String,
  subreddit: String,
  status: Number //undefined: new, 1: old
});
mongoose.model('reddit_comments', reddit_commentsSchema);