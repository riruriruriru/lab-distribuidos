const express = require('express')
const router = express.Router()
const Comment = require('../models/comments')
const Sentiment = require('../models/sentiment')

var mongoose = require('mongoose');
var CommentDB  = mongoose.model('reddit_comments');
var SentimentDB  = mongoose.model('sentiment');
//Get all users


exports.findAllComments = (req, res) => {
    CommentDB.find()
    .then(comments => {
        res.send(comments);
    }).catch(err => {
        res.status(404).send({
            message: err.message || "Some error occurred while retrieving comments."
        });
    });
};

exports.findAllAndUpdate = async (req, res) => {
    await CommentDB.updateMany({status: 1}, {status: 0});
   res.send("uwu")
};

exports.findCommentsStopwords = (req, res) => {
    CommentDB.find().sort({num_stopwords: -1}).limit(10)
    .then(comments => {
        res.send(comments);
    }).catch(err => {
        res.status(404).send({
            message: err.message || "Some error occurred while retrieving comments."
        });
    });
};
exports.findCommentsWords = (req, res) => {
    CommentDB.find().sort({total_words: -1}).limit(10)
    .then(comments => {
        res.send(comments);
    }).catch(err => {
        res.status(404).send({
            message: err.message || "Some error occurred while retrieving comments."
        });
    });
};
exports.findCommentsWordsSinStopwords = (req, res) => {
    CommentDB.find().sort({num_words_sin_stopwords: -1}).limit(10)
    .then(comments => {
        res.send(comments);
    }).catch(err => {
        res.status(404).send({
            message: err.message || "Some error occurred while retrieving comments."
        });
    });
};

exports.findCommentsSentences = (req, res) => {
    CommentDB.find().sort({total_sentences: -1}).limit(10)
    .then(comments => {
        res.send(comments);
    }).catch(err => {
        res.status(404).send({
            message: err.message || "Some error occurred while retrieving comments."
        });
    });
};

exports.saveSentiment = async(req, res)=>{
	const sentimentSave = new SentimentDB({ 
        pos: 0,
  		neg: 0,
        neu: 0,
        totalVowel: 0,
  totalConsonant: 0,
  totalUpper: 0,
  totalLower: 0,
  totalStopWords: 0,
  totalNonStopWords: 0,
  totalWords: 0,
  totalSentences: 0,
  totalUsers: 0,
  totalSubreddits: 0,
  totalPosts: 0,
  totalLanguages: 0,
  total: 0
    	});
    	sentimentSave.save()
    	.then(data => {
            res.send(data);
             }).catch(err => {
            res.status(404).send({
                message: err.message || "Some error occurred while creating the sentiment."
            });
        });    
}

exports.findPosNegNeu = async (req, res) => {
    var comentarios = await CommentDB.find({ status: { $ne: 1 } }).exec()
    console.log(comentarios.length)
    await CommentDB.updateMany({status: {$ne: 1}}, {status: 1});

    
    var sentiments = await SentimentDB.find().exec()
    var sentiment = sentiments[0].toObject()
    console.log(sentiment)
    var pos = 0
    var neg = 0
    var neu = 0
    var totalVowel = 0
    var totalConsonant = 0
    var totalUpper = 0
    var totalLower = 0
    var totalStopWords = 0
    var totalNonStopWords = 0
    var totalWords = 0
    var totalSentences = 0
    var totalUsers = [...new Set(comentarios.map(item => item.author))].length
    var totalSubreddit = [...new Set(comentarios.map(item => item.subreddit))].length
    const posts = new Set()
    const languages = new Set()
   
    var authorAgg = await CommentDB.aggregate([
    	 { $group: 
    	 { _id: '$author', total_comments: { $sum: 1 } 
  }},
    	 
    	 ]).sort({total_comments: -1}).limit(10).exec();
    var langAgg = await CommentDB.aggregate([
    	 { $group: 
    	 { _id: '$language', total_comments: { $sum: 1 }
  }},
    	 
    	 ]).sort({total_comments: -1}).limit(10).exec();
    var subredditAgg = await CommentDB.aggregate([
    	 { $group: 
    	 { _id: '$subreddit', total_comments: { $sum: 1 }
  }},
    	 
    	 ]).sort({total_comments: -1}).limit(10).exec();
    var submissionAgg = await CommentDB.aggregate([
    	 { $group: 
    	 { _id: '$post', total_comments: { $sum: 1 }
  }},
    	 
    	 ]).sort({total_comments: -1}).limit(10).exec();

  

    for(const comentario of comentarios){
    	var comment = comentario.toObject()
    	
    	totalVowel = totalVowel + comment.num_vowels
    	totalConsonant = totalConsonant + comment.num_consonants
    	totalUpper = totalUpper + comment.num_upper
    	totalLower = totalLower+ comment.num_lower
    	totalStopWords = totalStopWords + comment.num_stopwords
    	totalNonStopWords = totalNonStopWords + comment.num_words_sin_stopwords
    	totalWords = totalWords + comment.total_words
    	totalSentences = totalSentences +comment.total_sentences
    	posts.add(comment.post)
    	languages.add(comment.language)
   
    	
    	if(comment.pos>=comment.neu&&comment.pos>=comment.neg){
    		pos = pos+1
    	}
    	else if(comment.neu>=comment.pos&&comment.neu>=comment.neg){
    		neu = neu+1
    	}
    	else if(comment.neg>=comment.pos&&comment.neg>=comment.neu){
    		neg = neg+1
    	}
    }
    var totalLanguage = languages.size
    var totalSubmission = posts.size
    var sentPos =sentiment.pos+ pos
    var sentNeu =sentiment.neu+ neu
    var sentNeg =sentiment.neg+ neg
    var sentVowel =sentiment.totalVowel+ totalVowel
    var sentConsonant =sentiment.totalConsonant+ totalConsonant
    var sentUpper =sentiment.totalUpper+ totalUpper
    var sentLower =sentiment.totalLower+ totalLower
    var sentStopwords =sentiment.totalStopWords+ totalStopWords
    var sentNonStopWords =sentiment.totalNonStopWords+ totalNonStopWords
    var sentTotalWords =sentiment.totalWords+ totalWords
    var sentTotalSentences =sentiment.totalSentences+ totalSentences
    var sentTotalUsers =sentiment.totalUsers+ totalUsers
    var sentTotalSubreddits =sentiment.totalSubreddits+ totalSubreddit
    var sentPosts =sentiment.totalPosts+ totalSubmission
    var sentLanguages =sentiment.totalLanguages+ totalLanguage
    var sentTotal =sentiment.total+ comentarios.length
   
    await SentimentDB.findByIdAndUpdate(sentiment._id, 
    	{
    		  pos: sentPos,
			  neu: sentNeu,
			  neg: sentNeg, 
			  totalVowel: sentVowel,
			  totalConsonant: sentConsonant,
			  totalUpper: sentUpper,
			  totalLower: sentLower,
			  totalStopWords: sentStopwords,
			  totalNonStopWords: sentNonStopWords,
			  totalWords: sentTotalWords,
			  totalSentences: sentTotalSentences,
			  totalUsers: sentTotalUsers,
			  totalSubreddits: sentTotalSubreddits,
			  totalPosts: sentPosts,
			  totalLanguages: sentLanguages,
			  total: sentTotal,

    	}, {new: true, useFindAndModify: false}).exec()
    console.log("uwu")
    res.status(200).send({totalPos: sentPos, 
    	totalNeg: sentNeg, 
    	totalNeu: sentNeu, 
    	total: sentTotal, 
    	totalUsers: sentTotalUsers, 
    	totalLanguage: sentLanguages,
    	totalSubreddit: sentTotalSubreddits, 
    	totalVowel: sentVowel,
     totalConsonant: sentConsonant,
     totalUpper: sentUpper,
     totalLower: sentLower,
     totalStopWords: sentStopwords,
     totalNonStopWords: sentNonStopWords,
     totalWords: sentTotalWords,
     totalSentences: sentTotalSentences,
     totalSubmission: sentPosts,
     topAuthors: authorAgg,
     topSubreddit: subredditAgg,
     topLang: langAgg,
     topSubmission: submissionAgg})

    
};

exports.findByAuthors = async (req, res) => {
    var aggregation = await CommentDB.aggregate([
    	 { $group: 
    	 { _id: '$author', total_comments: { $sum: 1 } , num_stopwords: {$sum: '$num_stopwords'}, total_words: {$sum: '$total_words'}
    	 , total_sentences: {$sum: '$total_sentences'}, num_non_stopwords: {$sum: '$num_words_sin_stopwords'}, num_vowel: {$sum: '$num_vowels'},
    	 num_consonant: {$sum: '$num_consonants'}, num_upper: {$sum: '$num_upper'}, num_lower: {$sum: '$num_lower'},
    	avgPos: { $avg: "$pos" },avgNeu: { $avg: "$neu" }, avgNeg: { $avg: "$neg" },
        	countPos: { 
                $sum: { 
                    $switch: { 
                        "branches": [ 
                            { 
                                "case": { $and: [{$gte: [ "$pos", "$neu" ]}, {$gte: [ "$pos", "$neg" ]} ]}, 
                                "then": 1
                            }
                        ], 
                        "default": 0 
                    }
                }
            },
            countNeu: { 
                $sum: { 
                    $switch: { 
                        "branches": [ 
                            { 
                                "case": { $and: [{$gte: [ "$neu", "$pos" ]}, {$gte: [ "$neu", "$neg" ]} ]}, 
                                "then": 1
                            }
                        ], 
                        "default": 0 
                    }
                }
            },
            countNeg: { 
                $sum: { 
                    $switch: { 
                        "branches": [ 
                            { 
                                "case": { $and: [{$gte: [ "$neg", "$neu" ]}, {$gte: [ "$neg", "$pos" ]} ]}, 
                                "then": 1
                            }
                        ], 
                        "default": 0 
                    }
                }
            }} 
    	 },
    	 
    	 ]).sort({total_comments: -1}).limit(10).exec();

    res.status(200).send(aggregation)
};
exports.findBySubreddit = async (req, res) => {
    var aggregation = await CommentDB.aggregate([
    	 { $group: 
    	 { _id: '$subreddit', total_comments: { $sum: 1 }, num_stopwords: {$sum: '$num_stopwords'}, total_words: {$sum: '$total_words'}
    	 , total_sentences: {$sum: '$total_sentences'}, num_non_stopwords: {$sum: '$num_words_sin_stopwords'}, num_vowel: {$sum: '$num_vowels'},
    	 num_consonant: {$sum: '$num_consonants'}, num_upper: {$sum: '$num_upper'}, num_lower: {$sum: '$num_lower'},
    	 avgPos: { $avg: "$pos" },avgNeu: { $avg: "$neu" }, avgNeg: { $avg: "$neg" },
    	     	countPos: { 
                $sum: { 
                    $switch: { 
                        "branches": [ 
                            { 
                                "case": { $and: [{$gte: [ "$pos", "$neu" ]}, {$gte: [ "$pos", "$neg" ]} ]}, 
                                "then": 1
                            }
                        ], 
                        "default": 0 
                    }
                }
            },
            countNeu: { 
                $sum: { 
                    $switch: { 
                        "branches": [ 
                            { 
                                "case": { $and: [{$gte: [ "$neu", "$pos" ]}, {$gte: [ "$neu", "$neg" ]} ]}, 
                                "then": 1
                            }
                        ], 
                        "default": 0 
                    }
                }
            },
            countNeg: { 
                $sum: { 
                    $switch: { 
                        "branches": [ 
                            { 
                                "case": { $and: [{$gte: [ "$neg", "$neu" ]}, {$gte: [ "$neg", "$pos" ]} ]}, 
                                "then": 1
                            }
                        ], 
                        "default": 0 
                    }
                }
            } } 
    	 },
    	 
    	 ]).sort({total_comments: -1}).limit(10).exec();
  
    res.status(200).send(aggregation)
};
exports.findByPost = async (req, res) => {
    var aggregation = await CommentDB.aggregate([
    	 { $group: 
    	 { _id: '$post', total_comments: { $sum: 1 }, num_stopwords: {$sum: '$num_stopwords'}, total_words: {$sum: '$total_words'}
    	 , total_sentences: {$sum: '$total_sentences'}, num_non_stopwords: {$sum: '$num_words_sin_stopwords'}, num_vowel: {$sum: '$num_vowels'},
    	 num_consonant: {$sum: '$num_consonants'}, num_upper: {$sum: '$num_upper'}, num_lower: {$sum: '$num_lower'},
    	 avgPos: { $avg: "$pos" },avgNeu: { $avg: "$neu" }, avgNeg: { $avg: "$neg" },
    	     	countPos: { 
                $sum: { 
                    $switch: { 
                        "branches": [ 
                            { 
                                "case": { $and: [{$gte: [ "$pos", "$neu" ]}, {$gte: [ "$pos", "$neg" ]} ]}, 
                                "then": 1
                            }
                        ], 
                        "default": 0 
                    }
                }
            },
            countNeu: { 
                $sum: { 
                    $switch: { 
                        "branches": [ 
                            { 
                                "case": { $and: [{$gte: [ "$neu", "$pos" ]}, {$gte: [ "$neu", "$neg" ]} ]}, 
                                "then": 1
                            }
                        ], 
                        "default": 0 
                    }
                }
            },
            countNeg: { 
                $sum: { 
                    $switch: { 
                        "branches": [ 
                            { 
                                "case": { $and: [{$gte: [ "$neg", "$neu" ]}, {$gte: [ "$neg", "$pos" ]} ]}, 
                                "then": 1
                            }
                        ], 
                        "default": 0 
                    }
                }
            } } 
    	 },
    	 
    	 ]).sort({total_comments: -1}).limit(10).exec();
    
    res.status(200).send(aggregation)
};
exports.findByLanguage = async (req, res) => {
    var aggregation = await CommentDB.aggregate([
    	 { $group: 
    	 { _id: '$language', total_comments: { $sum: 1 }, total_comments: { $sum: 1 }, num_stopwords: {$sum: '$num_stopwords'}, total_words: {$sum: '$total_words'}
    	 , total_sentences: {$sum: '$total_sentences'}, num_non_stopwords: {$sum: '$num_words_sin_stopwords'}, num_vowel: {$sum: '$num_vowels'},
    	 num_consonant: {$sum: '$num_consonants'}, num_upper: {$sum: '$num_upper'}, num_lower: {$sum: '$num_lower'} ,
    	avgPos: { $avg: "$pos" },avgNeu: { $avg: "$neu" }, avgNeg: { $avg: "$neg" },
    	countPos: { 
                $sum: { 
                    $switch: { 
                        "branches": [ 
                            { 
                                "case": { $and: [{$gte: [ "$pos", "$neu" ]}, {$gte: [ "$pos", "$neg" ]} ]}, 
                                "then": 1
                            }
                        ], 
                        "default": 0 
                    }
                }
            },
            countNeu: { 
                $sum: { 
                    $switch: { 
                        "branches": [ 
                            { 
                                "case": { $and: [{$gte: [ "$neu", "$pos" ]}, {$gte: [ "$neu", "$neg" ]} ]}, 
                                "then": 1
                            }
                        ], 
                        "default": 0 
                    }
                }
            },
            countNeg: { 
                $sum: { 
                    $switch: { 
                        "branches": [ 
                            { 
                                "case": { $and: [{$gte: [ "$neg", "$neu" ]}, {$gte: [ "$neg", "$pos" ]} ]}, 
                                "then": 1
                            }
                        ], 
                        "default": 0 
                    }
                }
            }

    	  } 
    	 },
    	 
    	 ]).sort({total_comments: -1}).limit(10).exec();
 
    res.status(200).send(aggregation)
};
exports.wordAggregation = async (req, res) => {
    var comments = await CommentDB.find().exec()
    var words = []
    for(const comentario of comments){
    	//console.log(comment.body)
    	var comment = comentario.toObject()
		var wordArray = comment.words_sin_stopwords
    	for(const word of wordArray){
    		var newWord = word.toLowerCase()
    		if(words.filter(word => word.text === newWord).length===0){
  
    			words.push({text: newWord, value: 1})
    		}
    		else{
    			var indice = words.findIndex(word=>word.text===newWord)
    			var value = words[indice].value
    			words[indice] = {text: newWord, value: value+1}
    		}

    	}
    	
    	

    }
    var response = words.sort((a,b) => b.value-a.value)
    res.status(200).send(response.slice(0,50))
};

exports.stopWordsAggregation = async (req, res) => {
    var comments = await CommentDB.find().exec()
    var words = []
    for(const comentario of comments){
    	//console.log(comment.body)
    	var comment = comentario.toObject()
		var wordArray = comment.word_stopwords
		
	    	for(const word of wordArray){
	    		var newWord = word.toLowerCase()
	    		if(words.filter(word => word.text === newWord).length===0){
	  
	    			words.push({text: newWord, value: 1})
	    		}
	    		else{
	    			var indice = words.findIndex(word=>word.text===newWord)
	    			var value = words[indice].value
	    			words[indice] = {text: newWord, value: value+1}
	    		}

	    	}
    	
    	

    }
    var response = words.sort((a,b) => b.value-a.value)
    res.status(200).send(response.slice(0,50))
};