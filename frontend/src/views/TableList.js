/*!

=========================================================
* Black Dashboard React v1.1.0
=========================================================

* Product Page: https://www.creative-tim.com/product/black-dashboard-react
* Copyright 2020 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://github.com/creativetimofficial/black-dashboard-react/blob/master/LICENSE.md)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
import React from "react";
import axios from "axios";
// reactstrap components
import {
  Card,
  CardHeader,
  CardBody,
  CardTitle,
  Table,
  Row,
  Col
} from "reactstrap";

class Tables extends React.Component {
    constructor(props) {
    super(props);
    this.state = {
      stopWords: [],
      words: [],
      isLoading: true,
      showWordCloudStopWords: false,
      showWordCloudWords: false,
      topUsers: [],
      topSubreddits: [],
      topLanguages: [],
      positivos: 0,
      negativos: 0,
      neutrales: 0,
      total: 0,
      totalStopwords: 0,
      totalNotStopWords: 0,
      totalWords: 0,
      totalVowels: 0,
      totalConsonants: 0,
      totalLower: 0,
      totalUpper: 0,
      totalUsers: 0,
      totalPosts: 0,
      totalSubreddit: 0,
      totalLanguages: 0,
      topPosts: []
    };
  }
  componentDidMount(){
        axios.get(`http://35.188.17.251:9000/comments/sortBySubreddit`)
      .then(res => {
        console.log(res.data)
        var topSubreddits = res.data
        axios.get(`http://35.188.17.251:9000/comments/sortByAuthors`)
      .then(response => {
        console.log(response.data)
        var topUsers = response.data
        axios.get(`http://35.188.17.251:9000/comments/sortByLanguage`)
      .then(response2 => {
        console.log(response2.data)
        var languages = response2.data
        axios.get(`http://35.188.17.251:9000/comments/sortByPost`)
      .then(response3 => {
        console.log(response3.data)
        var post = response3.data
        this.setState({topUsers: topUsers, topSubreddits: topSubreddits, topLanguages: languages, isLoading: false, topPosts: post})
      })
        
      })
    })
    })
  }

  render() {
    const topUsers = this.state.topUsers
    const topSubreddits = this.state.topSubreddits
    const topLanguages = this.state.topLanguages
    const topPosts = this.state.topPosts
    if(this.state.isLoading===true){
      return(<div className="content" > <p>Cargando...</p></div>)
    }
    return (
      <>
        <div className="content">
          <Row>

              <Card>
                <CardHeader>
                  <CardTitle tag="h4">Top Subreddits</CardTitle>
                </CardHeader>
                <CardBody>
                  <Table className="tablesorter" responsive>
                    <thead className="text-primary">
                      <tr>
                        <th>Nombre</th>
                        <th>Comentarios</th>
                        <th>Palabras</th>
                        <th>Stop Words</th>
                        <th>Mayúsculas</th>
                        <th>Minúsculas</th>
                        <th>Consonantes</th>
                        <th>Vocales</th>
                        <th>Oraciones</th>
                        <th>Positivos</th>
                        <th>Neutrales</th>
                        <th>Negativos</th>
                      
                      </tr>
                    </thead>
                    <tbody>
                    {topSubreddits.map((subreddit, i)=> 

                      <tr>
                      <td>{subreddit._id}</td>
                      <td>{subreddit.total_comments}</td>
                      <td>{subreddit.total_words}</td>
                      <td>{subreddit.num_stopwords}</td>
                      <td>{subreddit.num_upper}</td>
                      <td>{subreddit.num_lower}</td>
                      <td>{subreddit.num_consonant}</td>
                      <td>{subreddit.num_vowel}</td>
                      <td>{subreddit.total_sentences}</td>
                      <td>{subreddit.countPos}</td>
                      <td>{subreddit.countNeu}</td>
                      <td>{subreddit.countNeg}</td>
                      </tr>

                      
                    )}
                    </tbody>
                  </Table>
                </CardBody>
              </Card>
           
              <Card>
                <CardHeader>
                  <CardTitle tag="h4">Top Users</CardTitle>
                </CardHeader>
                <CardBody>
                  <Table className="tablesorter" responsive>
                    <thead className="text-primary">
                      <tr>
                         <th>Nombre</th>
                        <th>Comentarios</th>
                        <th>Palabras</th>
                        <th>Stop Words</th>
                        <th>Mayúsculas</th>
                        <th>Minúsculas</th>
                        <th>Consonantes</th>
                        <th>Vocales</th>
                        <th>Oraciones</th>
                        <th>Positivos</th>
                        <th>Neutrales</th>
                        <th>Negativos</th>
                      
                      </tr>
                    </thead>
                    <tbody>
                    {topUsers.map((user, i)=> 

                      <tr>
                     <td>{user._id}</td>
                      <td>{user.total_comments}</td>
                      <td>{user.total_words}</td>
                      <td>{user.num_stopwords}</td>
                      <td>{user.num_upper}</td>
                      <td>{user.num_lower}</td>
                      <td>{user.num_consonant}</td>
                      <td>{user.num_vowel}</td>
                      <td>{user.total_sentences}</td>
                      <td>{user.countPos}</td>
                      <td>{user.countNeu}</td>
                      <td>{user.countNeg}</td>
                      </tr>

                      
                    )}
                    </tbody>
                  </Table>
                </CardBody>
              </Card>
               <Card>
                <CardHeader>
                  <CardTitle tag="h4">Top Idiomas</CardTitle>
                </CardHeader>
                <CardBody>
                  <Table className="tablesorter" responsive>
                    <thead className="text-primary">
                      <tr>
                         <th>Nombre</th>
                        <th>Comentarios</th>
                        <th>Palabras</th>
                        <th>Stop Words</th>
                        <th>Mayúsculas</th>
                        <th>Minúsculas</th>
                        <th>Consonantes</th>
                        <th>Vocales</th>
                        <th>Oraciones</th>
                        <th>Positivos</th>
                        <th>Neutrales</th>
                        <th>Negativos</th>
                      
                      </tr>
                    </thead>
                    <tbody>
                    {topLanguages.map((language, i)=> 

                      <tr>
                     <td>{language._id===" " ? "Sin determinar":language._id}</td>
                      <td>{language.total_comments}</td>
                      <td>{language.total_words}</td>
                      <td>{language.num_stopwords}</td>
                      <td>{language.num_upper}</td>
                      <td>{language.num_lower}</td>
                      <td>{language.num_consonant}</td>
                      <td>{language.num_vowel}</td>
                      <td>{language.total_sentences}</td>
                      <td>{language.countPos}</td>
                      <td>{language.countNeu}</td>
                      <td>{language.countNeg}</td>
                      </tr>

                      
                    )}
                    </tbody>
                  </Table>
                </CardBody>
              </Card>
              <Card>
                <CardHeader>
                  <CardTitle tag="h4">Top Posts</CardTitle>
                </CardHeader>
                <CardBody>
                  <Table className="tablesorter" responsive>
                    <thead className="text-primary">
                      <tr>
                         <th>Nombre</th>
                        <th>Comentarios</th>
                        <th>Palabras</th>
                        <th>Stop Words</th>
                        <th>Mayúsculas</th>
                        <th>Minúsculas</th>
                        <th>Consonantes</th>
                        <th>Vocales</th>
                        <th>Oraciones</th>
                        <th>Positivos</th>
                        <th>Neutrales</th>
                        <th>Negativos</th>
                      
                      </tr>
                    </thead>
                    <tbody>
                    {topPosts.map((post, i)=> 

                      <tr>
                     <td>{post._id===" " ? "Sin determinar":post._id}</td>
                      <td>{post.total_comments}</td>
                      <td>{post.total_words}</td>
                      <td>{post.num_stopwords}</td>
                      <td>{post.num_upper}</td>
                      <td>{post.num_lower}</td>
                      <td>{post.num_consonant}</td>
                      <td>{post.num_vowel}</td>
                      <td>{post.total_sentences}</td>
                      <td>{post.countPos}</td>
                      <td>{post.countNeu}</td>
                      <td>{post.countNeg}</td>
                      </tr>

                      
                    )}
                    </tbody>
                  </Table>
                </CardBody>
              </Card>
          </Row>
        </div>
      </>
    );
  }
}

export default Tables;
