/*!

=========================================================
* Black Dashboard React v1.1.0
=========================================================

* Product Page: https://www.creative-tim.com/product/black-dashboard-react
* Copyright 2020 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://github.com/creativetimofficial/black-dashboard-react/blob/master/LICENSE.md)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
import React from "react";
// nodejs library that concatenates classes
import classNames from "classnames";
// react plugin used to create charts
import { Line, Bar } from "react-chartjs-2";
import ReactWordcloud from 'react-wordcloud';
// reactstrap components
import axios from 'axios';
import {
  Button,
  ButtonGroup,
  Card,
  CardHeader,
  CardBody,
  CardTitle,
  CardFooter,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  UncontrolledDropdown,
  Label,
  FormGroup,
  Input,
  Table,
  Row,
  Col,
  UncontrolledTooltip
} from "reactstrap";

// core components

const chart1_2_options = {
  maintainAspectRatio: false,
  legend: {
    display: false
  },
  tooltips: {
    backgroundColor: "#f5f5f5",
    titleFontColor: "#333",
    bodyFontColor: "#666",
    bodySpacing: 4,
    xPadding: 12,
    mode: "nearest",
    intersect: 0,
    position: "nearest"
  },
  responsive: true,
  scales: {
    yAxes: [
      {
        barPercentage: 1.6,
        gridLines: {
          drawBorder: false,
          color: "rgba(29,140,248,0.0)",
          zeroLineColor: "transparent"
        },
        ticks: {
          suggestedMin: 60,
          suggestedMax: 125,
          padding: 20,
          fontColor: "#9a9a9a"
        }
      }
    ],
    xAxes: [
      {
        barPercentage: 1.6,
        gridLines: {
          drawBorder: false,
          color: "rgba(29,140,248,0.1)",
          zeroLineColor: "transparent"
        },
        ticks: {
          padding: 20,
          fontColor: "#9a9a9a"
        }
      }
    ]
  }
};

const options = {
  colors: ["#1f77b4", "#ff7f0e", "#2ca02c", "#d62728", "#9467bd", "#8c564b"],
  enableTooltip: true,
  deterministic: false,
  fontFamily: "impact",
  fontSizes: [5, 60],
  fontStyle: "normal",
  fontWeight: "normal",
  padding: 1,
  rotations: 3,
  rotationAngles: [0, 90],
  scale: "sqrt",
  spiral: "archimedean",
  transitionDuration: 1000
};
class Dashboard extends React.Component {
  constructor(props) {
    super(props);
    this.handleMostrarWordCloud = this.handleMostrarWordCloud.bind(this)
    this.obtainSentiment = this.obtainSentiment.bind(this);
    this.setData = this.setData.bind(this)
    this.setData2 = this.setData2.bind(this)
    this.setData3 = this.setData3.bind(this)
    this.setData4 = this.setData4.bind(this)
    this.setData5 = this.setData5.bind(this)
    this.state = {
      bigChartData: "data1",
      stopWords: [],
      words: [],
      isLoading: true,
      showWordCloudStopWords: false,
      showWordCloudWords: false,
      topUsers: [],
      topSubreddits: [],
      topLanguages: [],
      positivos: 0,
      negativos: 0,
      neutrales: 0,
      total: 0,
      totalStopWords: 0,
      totalNotStopWords: 0,
      totalWords: 0,
      totalVowels: 0,
      totalConsonants: 0,
      totalLower: 0,
      totalUpper: 0,
      totalUsers: 0,
      totalPosts: 0,
      totalSubreddit: 0,
      totalLanguages: 0,
      totalSentences: 0,
      topSubmission: []
    };
  }
  setBgChartData = name => {
    this.setState({
      bigChartData: name
    });
  };
  setData3(){

    const chartExample4 = {
  data: canvas => {
    let ctx = canvas.getContext("2d");

    let gradientStroke = ctx.createLinearGradient(0, 230, 0, 50);

    gradientStroke.addColorStop(1, "rgba(66,134,121,0.15)");
    gradientStroke.addColorStop(0.4, "rgba(66,134,121,0.0)"); //green colors
    gradientStroke.addColorStop(0, "rgba(66,134,121,0)"); //green colors

    return {
      labels:  [this.state.topSubreddits[0]._id, this.state.topSubreddits[1]._id, this.state.topSubreddits[2]._id, this.state.topSubreddits[3]._id, this.state.topSubreddits[4]._id, this.state.topSubreddits[5]._id,
      this.state.topSubreddits[6]._id,this.state.topSubreddits[7]._id,this.state.topSubreddits[8]._id,this.state.topSubreddits[9]._id],
      datasets: [
        {
          label: "My First dataset",
          fill: true,
          backgroundColor: gradientStroke,
          borderColor: "#00d6b4",
          borderWidth: 2,
          borderDash: [],
          borderDashOffset: 0.0,
          pointBackgroundColor: "#00d6b4",
          pointBorderColor: "rgba(255,255,255,0)",
          pointHoverBackgroundColor: "#00d6b4",
          pointBorderWidth: 20,
          pointHoverRadius: 4,
          pointHoverBorderWidth: 15,
          pointRadius: 4,
          data: [this.state.topSubreddits[0].total_comments, this.state.topSubreddits[1].total_comments, this.state.topSubreddits[2].total_comments, this.state.topSubreddits[3].total_comments, this.state.topSubreddits[4].total_comments, this.state.topSubreddits[5].total_comments,
      this.state.topSubreddits[6].total_comments,this.state.topSubreddits[7].total_comments,this.state.topSubreddits[8].total_comments,this.state.topSubreddits[9].total_comments]
        }
      ]
    };
  },
  options: {
    maintainAspectRatio: false,
    legend: {
      display: false
    },

    tooltips: {
      backgroundColor: "#f5f5f5",
      titleFontColor: "#333",
      bodyFontColor: "#666",
      bodySpacing: 4,
      xPadding: 12,
      mode: "nearest",
      intersect: 0,
      position: "nearest"
    },
    responsive: true,
    scales: {
      yAxes: [
        {
          barPercentage: 1.6,
          gridLines: {
            drawBorder: false,
            color: "rgba(29,140,248,0.0)",
            zeroLineColor: "transparent"
          },
          ticks: {
            suggestedMin: 50,
            suggestedMax: 125,
            padding: 20,
            fontColor: "#9e9e9e"
          }
        }
      ],

      xAxes: [
        {
          barPercentage: 1.6,
          gridLines: {
            drawBorder: false,
            color: "rgba(0,242,195,0.1)",
            zeroLineColor: "transparent"
          },
          ticks: {
            padding: 20,
            fontColor: "#9e9e9e"
          }
        }
      ]
    }
  }
};
return chartExample4
  }
  setData2(){
    console.log("set data 2")
    console.log("top userds: ", this.state.topUsers)
    let chartExample2 = {
  data: canvas => {
    let ctx = canvas.getContext("2d");

    let gradientStroke = ctx.createLinearGradient(0, 230, 0, 50);

    gradientStroke.addColorStop(1, "rgba(29,140,248,0.2)");
    gradientStroke.addColorStop(0.4, "rgba(29,140,248,0.0)");
    gradientStroke.addColorStop(0, "rgba(29,140,248,0)"); //blue colors

    return {
      labels: [this.state.topUsers[0]._id, this.state.topUsers[1]._id, this.state.topUsers[2]._id, this.state.topUsers[3]._id, this.state.topUsers[4]._id, this.state.topUsers[5]._id,
      this.state.topUsers[6]._id,this.state.topUsers[7]._id,this.state.topUsers[8]._id,this.state.topUsers[9]._id],
      datasets: [
        {
          label: "Top Usuarios",
          fill: true,
          backgroundColor: gradientStroke,
          borderColor: "#1f8ef1",
          borderWidth: 2,
          borderDash: [],
          borderDashOffset: 0.0,
          pointBackgroundColor: "#1f8ef1",
          pointBorderColor: "rgba(255,255,255,0)",
          pointHoverBackgroundColor: "#1f8ef1",
          pointBorderWidth: 20,
          pointHoverRadius: 4,
          pointHoverBorderWidth: 15,
          pointRadius: 4,
          data: [this.state.topUsers[0].total_comments, this.state.topUsers[1].total_comments, this.state.topUsers[2].total_comments, this.state.topUsers[3].total_comments, this.state.topUsers[4].total_comments, this.state.topUsers[5].total_comments,
      this.state.topUsers[6].total_comments,this.state.topUsers[7].total_comments,this.state.topUsers[8].total_comments,this.state.topUsers[9].total_comments]
        }
      ]
    };
  },
  options: chart1_2_options
};
  return chartExample2  
  }
   setData4(){
    console.log("set data 2")
    console.log("top userds: ", this.state.topUsers)
    let chartExample5 = {
  data: canvas => {
    let ctx = canvas.getContext("2d");

    let gradientStroke = ctx.createLinearGradient(0, 230, 0, 50);

    gradientStroke.addColorStop(1, "rgba(29,140,248,0.2)");
    gradientStroke.addColorStop(0.4, "rgba(29,140,248,0.0)");
    gradientStroke.addColorStop(0, "rgba(29,140,248,0)"); //blue colors

    return {
      labels: [this.state.topSubmission[0]._id.slice(0, 15), this.state.topSubmission[1]._id.slice(0, 15), this.state.topSubmission[2]._id.slice(0, 15), this.state.topSubmission[3]._id.slice(0, 15), this.state.topSubmission[4]._id.slice(0, 15), this.state.topSubmission[5]._id.slice(0, 15),
      this.state.topSubmission[6]._id.slice(0, 15),this.state.topSubmission[7]._id.slice(0, 15),this.state.topSubmission[8]._id.slice(0, 15),this.state.topSubmission[9]._id.slice(0, 15)],
      datasets: [
        {
          label: "Top Posts",
          fill: true,
          backgroundColor: gradientStroke,
          borderColor: "#1f8ef1",
          borderWidth: 2,
          borderDash: [],
          borderDashOffset: 0.0,
          pointBackgroundColor: "#1f8ef1",
          pointBorderColor: "rgba(255,255,255,0)",
          pointHoverBackgroundColor: "#1f8ef1",
          pointBorderWidth: 20,
          pointHoverRadius: 4,
          pointHoverBorderWidth: 15,
          pointRadius: 4,
          data: [this.state.topSubmission[0].total_comments, this.state.topSubmission[1].total_comments, this.state.topSubmission[2].total_comments, this.state.topSubmission[3].total_comments, this.state.topSubmission[4].total_comments, this.state.topSubmission[5].total_comments,
      this.state.topSubmission[6].total_comments,this.state.topSubmission[7].total_comments,this.state.topSubmission[8].total_comments,this.state.topSubmission[9].total_comments]
        }
      ]
    };
  },
  options: {
    maintainAspectRatio: false,
    legend: {
      display: false
    },

    tooltips: {
      backgroundColor: "#f5f5f5",
      titleFontColor: "#333",
      bodyFontColor: "#666",
      bodySpacing: 4,
      xPadding: 12,
      mode: "nearest",
      intersect: 0,
      position: "nearest"
    },
    responsive: true,
    scales: {
      yAxes: [
        {
          barPercentage: 1.6,
          gridLines: {
            drawBorder: false,
            color: "rgba(29,140,248,0.0)",
            zeroLineColor: "transparent"
          },
          ticks: {
            suggestedMin: 50,
            suggestedMax: 125,
            padding: 20,
            fontColor: "#9e9e9e"
          }
        }
      ],

      xAxes: [
        {
          barPercentage: 1.6,
          gridLines: {
            drawBorder: false,
            color: "rgba(0,242,195,0.1)",
            zeroLineColor: "transparent"
          },
          ticks: {
            padding: 20,
            fontColor: "#9e9e9e"
          }
        }
      ]
    }
  }
};
  return chartExample5  
  }
  setData(){
  let chartExample1 = {
  data1: canvas => {
    let ctx = canvas.getContext("2d");

    let gradientStroke = ctx.createLinearGradient(0, 230, 0, 50);

    gradientStroke.addColorStop(1, "rgba(29,140,248,0.2)");
    gradientStroke.addColorStop(0.4, "rgba(29,140,248,0.0)");
    gradientStroke.addColorStop(0, "rgba(29,140,248,0)"); //blue colors

    return {
      labels: [
        "Usuarios",
        "Subreddits",
        "Posts",
        "Idiomas"

        
      ],
      datasets: [
        {
          label: "Usuarios, Subreddits, Posts e Idiomas",
          fill: true,
          backgroundColor: gradientStroke,
          borderColor: "#1f8ef1",
          borderWidth: 2,
          borderDash: [],
          borderDashOffset: 0.0,
          pointBackgroundColor: "#1f8ef1",
          pointBorderColor: "rgba(255,255,255,0)",
          pointHoverBackgroundColor: "#1f8ef1",
          pointBorderWidth: 20,
          pointHoverRadius: 4,
          pointHoverBorderWidth: 15,
          pointRadius: 4,
          data: [this.state.totalUsers, this.state.totalSubreddit, this.state.totalPosts, this.state.totalLanguages]
        }
      ]
    };
  },
  data2: canvas => {
    let ctx = canvas.getContext("2d");

    let gradientStroke = ctx.createLinearGradient(0, 230, 0, 50);

    gradientStroke.addColorStop(1, "rgba(29,140,248,0.2)");
    gradientStroke.addColorStop(0.4, "rgba(29,140,248,0.0)");
    gradientStroke.addColorStop(0, "rgba(29,140,248,0)"); //blue colors

    return {
      labels: [
        "Vocales",
        "Consonantes",
        "Mayúsculas",
        "Minúsculas",
        
      ],
      datasets: [
        {
          label: "Vocales, Consonantes, Mayúsculas y Minúsculas",
          fill: true,
          backgroundColor: gradientStroke,
          borderColor: "#1f8ef1",
          borderWidth: 2,
          borderDash: [],
          borderDashOffset: 0.0,
          pointBackgroundColor: "#1f8ef1",
          pointBorderColor: "rgba(255,255,255,0)",
          pointHoverBackgroundColor: "#1f8ef1",
          pointBorderWidth: 20,
          pointHoverRadius: 4,
          pointHoverBorderWidth: 15,
          pointRadius: 4,
          data: [this.state.totalVowels, this.state.totalConsonants, this.state.totalUpper, this.state.totalLower]
        }
      ]
    };
  },
  data3: canvas => {
    let ctx = canvas.getContext("2d");

    let gradientStroke = ctx.createLinearGradient(0, 230, 0, 50);

    gradientStroke.addColorStop(1, "rgba(29,140,248,0.2)");
    gradientStroke.addColorStop(0.4, "rgba(29,140,248,0.0)");
    gradientStroke.addColorStop(0, "rgba(29,140,248,0)"); //blue colors

    return {
      labels: [
        "Palabras (Total)",
        "Palabras No Stopwords",
        "Stop Words",
        "Oraciones",
        "Total Comentarios"

      ],
      datasets: [
        {
          label: "Palabras, Stopwords, Oraciones y Comentarios",
          fill: true,
          backgroundColor: gradientStroke,
          borderColor: "#1f8ef1",
          borderWidth: 2,
          borderDash: [],
          borderDashOffset: 0.0,
          pointBackgroundColor: "#1f8ef1",
          pointBorderColor: "rgba(255,255,255,0)",
          pointHoverBackgroundColor: "#1f8ef1",
          pointBorderWidth: 20,
          pointHoverRadius: 4,
          pointHoverBorderWidth: 15,
          pointRadius: 4,
          data: [this.state.totalWords, this.state.totalNotStopWords, this.state.totalStopWords,  this.state.totalSentences, this.state.total]
        }
      ]
    };
  },
  options: chart1_2_options
}
return chartExample1
} 
   obtainSentiment(pos, neg, neu){


          let chartExample3 = {
        data: canvas => {
          let ctx = canvas.getContext("2d");

          let gradientStroke = ctx.createLinearGradient(0, 230, 0, 50);

          gradientStroke.addColorStop(1, "rgba(72,72,176,0.1)");
          gradientStroke.addColorStop(0.4, "rgba(72,72,176,0.0)");
          gradientStroke.addColorStop(0, "rgba(119,52,169,0)"); //purple colors
          

          return {
            labels: ["Positivos", "Negativos", "Neutrales"],
            datasets: [
              {
                label: "Cantidad",
                fill: true,
                backgroundColor: gradientStroke,
                hoverBackgroundColor: gradientStroke,
                borderColor: "#d048b6",
                borderWidth: 2,
                borderDash: [],
                borderDashOffset: 0.0,
                data: [pos, neg, neu]
              }
            ]
          };
        },
        options: {
          maintainAspectRatio: false,
          legend: {
            display: false
          },
          tooltips: {
            backgroundColor: "#f5f5f5",
            titleFontColor: "#333",
            bodyFontColor: "#666",
            bodySpacing: 4,
            xPadding: 12,
            mode: "nearest",
            intersect: 0,
            position: "nearest"
          },
          responsive: true,
          scales: {
            yAxes: [
              {
                gridLines: {
                  drawBorder: false,
                  color: "rgba(225,78,202,0.1)",
                  zeroLineColor: "transparent"
                },
                ticks: {
                  suggestedMin: 60,
                  suggestedMax: 120,
                  padding: 20,
                  fontColor: "#9e9e9e"
                }
              }
            ],
            xAxes: [
              {
                gridLines: {
                  drawBorder: false,
                  color: "rgba(225,78,202,0.1)",
                  zeroLineColor: "transparent"
                },
                ticks: {
                  padding: 20,
                  fontColor: "#9e9e9e"
                }
              }
            ]
          }
        }
      };
      return chartExample3
  
  }

    setData5(){


          let chartExample6 = {
        data: canvas => {
          let ctx = canvas.getContext("2d");

          let gradientStroke = ctx.createLinearGradient(0, 230, 0, 50);

          gradientStroke.addColorStop(1, "rgba(72,72,176,0.1)");
          gradientStroke.addColorStop(0.4, "rgba(72,72,176,0.0)");
          gradientStroke.addColorStop(0, "rgba(119,52,169,0)"); //purple colors
          

          return {
            labels: ["Sin Determinar", this.state.topLanguages[1]._id, this.state.topLanguages[2]._id, this.state.topLanguages[3]._id, this.state.topLanguages[4]._id, this.state.topLanguages[5]._id,
      this.state.topLanguages[6]._id,this.state.topLanguages[7]._id,this.state.topLanguages[8]._id,this.state.topLanguages[9]._id],
            datasets: [
              {
                label: "Cantidad",
                fill: true,
                backgroundColor: gradientStroke,
                hoverBackgroundColor: gradientStroke,
                borderColor: "#d048b6",
                borderWidth: 2,
                borderDash: [],
                borderDashOffset: 0.0,
                data: 
      [this.state.topLanguages[0].total_comments, this.state.topLanguages[1].total_comments, this.state.topLanguages[2].total_comments, this.state.topLanguages[3].total_comments, this.state.topLanguages[4].total_comments, this.state.topLanguages[5].total_comments,
      this.state.topLanguages[6].total_comments,this.state.topLanguages[7].total_comments,this.state.topLanguages[8].total_comments,this.state.topLanguages[9].total_comments]
              }
            ]
          };
        },
        options: {
          maintainAspectRatio: false,
          legend: {
            display: false
          },
          tooltips: {
            backgroundColor: "#f5f5f5",
            titleFontColor: "#333",
            bodyFontColor: "#666",
            bodySpacing: 4,
            xPadding: 12,
            mode: "nearest",
            intersect: 0,
            position: "nearest"
          },
          responsive: true,
          scales: {
            yAxes: [
              {
                gridLines: {
                  drawBorder: false,
                  color: "rgba(225,78,202,0.1)",
                  zeroLineColor: "transparent"
                },
                ticks: {
                  suggestedMin: 60,
                  suggestedMax: 120,
                  padding: 20,
                  fontColor: "#9e9e9e"
                }
              }
            ],
            xAxes: [
              {
                gridLines: {
                  drawBorder: false,
                  color: "rgba(225,78,202,0.1)",
                  zeroLineColor: "transparent"
                },
                ticks: {
                  padding: 20,
                  fontColor: "#9e9e9e"
                }
              }
            ]
          }
        }
      };
      return chartExample6
  
  }

  handleMostrarWordCloud(i){
    if(i===1&&this.state.showWordCloudStopWords===false){
      this.setState({showWordCloudStopWords: true})
      axios.get(`http://35.188.17.251:9000/comments/countStopWords`)
      .then(res => {
        console.log(res.data)
        this.setState({stopWords: res.data, showWordCloudStopWords: true });
      })
    }
    else if(i===2&&this.state.showWordCloudWords===false){
      this.setState({showWordCloudWords: true})
      axios.get(`http://35.188.17.251:9000/comments/countWords`)
      .then(res => {
        console.log(res.data)
        this.setState({words: res.data, showWordCloudWords: true });
      })
    }


  }
  componentDidMount(){
    

        axios.get(`http://35.188.17.251:9000/comments/sentiment`)
          .then(response3=> {
            var datos = response3.data
            console.log(datos)
            this.setState({isLoading: false, positivos: datos.totalPos, negativos: datos.totalNeg, neutrales: datos.totalNeu, total: datos.total,
                  totalStopWords: datos.totalStopWords,
                  totalNotStopWords: datos.totalNonStopWords,
                  totalWords: datos.totalWords,
                  totalVowels: datos.totalVowel,
                  totalConsonants: datos.totalConsonant,
                  totalLower: datos.totalLower,
                  totalUpper: datos.totalUpper,
                  totalUsers: datos.totalUsers,
                  totalPosts: datos.totalSubmission,
                  totalSubreddit: datos.totalSubreddit,
                  totalLanguages: datos.totalLanguage,
                  totalSentences: datos.totalSentences,
                  topUsers: datos.topAuthors,
                  topLanguages: datos.topLang,
                  topSubreddits: datos.topSubreddit,
                  topSubmission: datos.topSubmission});

          })

 
  }
  render() {
    const stopWords = this.state.stopWords
    const isLoading = this.state.isLoading
    const words = this.state.words
    const chartExample3 = this.obtainSentiment(this.state.positivos, this.state.negativos,this.state.neutrales)
    const chartExample1 = this.setData()
    const chartExample2 = this.setData2()
    const chartExample4 = this.setData3()
    const chartExample5 = this.setData4()
    const chartExample6 = this.setData5()
    if(isLoading===true){
      return(<div className="content"> <p>Cargando...</p></div>)
    }
    return (
      <>
        <div className="content">
          <Row>
            <Col xs="12">
              <Card className="card-chart">
                <CardHeader>
                  <Row>
                    <Col className="text-left" sm="6">
                      <h5 className="card-category">Análisis de Comentarios</h5>
                      <CardTitle tag="h2">Total de Comentarios: {this.state.total}</CardTitle>
                    </Col>
                    <Col sm="6">
                      <ButtonGroup
                        className="btn-group-toggle float-right"
                        data-toggle="buttons"
                      >
                        <Button
                          tag="label"
                          className={classNames("btn-simple", {
                            active: this.state.bigChartData === "data1"
                          })}
                          color="info"
                          id="0"
                          size="sm"
                          onClick={() => this.setBgChartData("data1")}
                        >
                          <input
                            defaultChecked
                            className="d-none"
                            name="options"
                            type="radio"
                          />
                          <span className="d-none d-sm-block d-md-block d-lg-block d-xl-block">
                            Reddit
                          </span>
                          <span className="d-block d-sm-none">
                            <i className="tim-icons icon-single-02" />
                          </span>
                        </Button>
                        <Button
                          color="info"
                          id="1"
                          size="sm"
                          tag="label"
                          className={classNames("btn-simple", {
                            active: this.state.bigChartData === "data2"
                          })}
                          onClick={() => this.setBgChartData("data2")}
                        >
                          <input
                            className="d-none"
                            name="options"
                            type="radio"
                          />
                          <span className="d-none d-sm-block d-md-block d-lg-block d-xl-block">
                            Análisis de Texto (1)
                          </span>
                          <span className="d-block d-sm-none">
                            <i className="tim-icons icon-gift-2" />
                          </span>
                        </Button>
                        <Button
                          color="info"
                          id="2"
                          size="sm"
                          tag="label"
                          className={classNames("btn-simple", {
                            active: this.state.bigChartData === "data3"
                          })}
                          onClick={() => this.setBgChartData("data3")}
                        >
                          <input
                            className="d-none"
                            name="options"
                            type="radio"
                          />
                          <span className="d-none d-sm-block d-md-block d-lg-block d-xl-block">
                            Análisis de Texto (2)
                          </span>
                          <span className="d-block d-sm-none">
                            <i className="tim-icons icon-tap-02" />
                          </span>
                        </Button>
                      </ButtonGroup>
                    </Col>
                  </Row>
                </CardHeader>
                <CardBody>
                  <div className="chart-area">
                    <Line
                      data={chartExample1[this.state.bigChartData]}
                      options={chartExample1.options}
                    />
                  </div>
                </CardBody>
              </Card>
              <Row>
              <Card className="card-chart">
              
               <CardTitle tag="h2">WordCloud StopWords</CardTitle>
               {this.state.showWordCloudStopWords===true
                ?
                             <ReactWordcloud words={stopWords} options = {options}/>
                  : null}
              <CardFooter>
               <Button
                          tag="label"
                          className={classNames("btn-simple", {
                            active: this.state.bigChartData === "data1"
                          })}
                          color="info"
                          id="0"
                          size="sm"
                          onClick={() => this.handleMostrarWordCloud(1)}
                        >  Mostrar WordCloud de Stop Words
                        </Button>
              </CardFooter>
              </Card>
              </Row>
              <Row>
              <Card className="card-chart">
              
               <CardTitle tag="h2">WordCloud de Palabras</CardTitle>
               {this.state.showWordCloudWords===true
                ?
                             <ReactWordcloud words={words} options = {options}/>
                  : null}
              <CardFooter>
               <Button
                          tag="label"
                          className={classNames("btn-simple", {
                            active: this.state.bigChartData === "data1"
                          })}
                          color="info"
                          id="0"
                          size="sm"
                          onClick={() => this.handleMostrarWordCloud(2)}
                        >  Mostrar WordCloud de Stop Words
                        </Button>
              </CardFooter>
              </Card>
              </Row>
            </Col>
          </Row>
          <Row>
            <Col lg="4">
              <Card className="card-chart">
                <CardHeader>
                  <h5 className="card-category">Top Usuarios</h5>
                  <CardTitle tag="h3">
                    <i className="tim-icons icon-bell-55 text-info" />{" "}
                    {this.state.topUsers[0].total_comments+this.state.topUsers[1].total_comments+this.state.topUsers[2].total_comments+
                      this.state.topUsers[3].total_comments+this.state.topUsers[4].total_comments+this.state.topUsers[5].total_comments+
                      this.state.topUsers[6].total_comments+this.state.topUsers[7].total_comments+this.state.topUsers[8].total_comments+
                      this.state.topUsers[9].total_comments}
                  </CardTitle>
                </CardHeader>
                <CardBody>
                  <div className="chart-area">
                    <Line
                      data={chartExample2.data}
                      options={chartExample2.options}
                    />
                  </div>
                </CardBody>
              </Card>
            </Col>
            <Col lg="4">
              <Card className="card-chart">
                <CardHeader>
                  <h5 className="card-category">Análisis de Sentimientos</h5>
                  <CardTitle tag="h3">
                    <i className="tim-icons icon-delivery-fast text-primary" />{" "}
                    Comentarios Analizados: {this.state.total}
                  </CardTitle>
                </CardHeader>
                <CardBody>
                  <div className="chart-area">
                    <Bar
                      data={chartExample3.data}
                      options={chartExample3.options}
                    />
                  </div>
                </CardBody>
              </Card>
            </Col>
            <Col lg="4">
              <Card className="card-chart">
                <CardHeader>
                  <h5 className="card-category">Top Subreddits</h5>
                  <CardTitle tag="h3">
                    <i className="tim-icons icon-send text-success" />   {this.state.topSubreddits[0].total_comments+this.state.topSubreddits[1].total_comments+this.state.topSubreddits[2].total_comments+
                      this.state.topSubreddits[3].total_comments+this.state.topSubreddits[4].total_comments+this.state.topSubreddits[5].total_comments+
                      this.state.topSubreddits[6].total_comments+this.state.topSubreddits[7].total_comments+this.state.topSubreddits[8].total_comments+
                      this.state.topSubreddits[9].total_comments}
                  </CardTitle>
                </CardHeader>
                <CardBody>
                  <div className="chart-area">
                    <Line
                      data={chartExample4.data}
                      options={chartExample4.options}
                    />
                  </div>
                </CardBody>
              </Card>
            </Col>
          </Row>
          <Row>
            <Col >
              <Card className="card-chart">
                <CardHeader>
                  <h5 className="card-category">Top Submissions</h5>
                  <CardTitle tag="h3">
                    <i className="tim-icons icon-bell-55 text-info" />{" "}
                    {this.state.topSubmission[0].total_comments+this.state.topSubmission[1].total_comments+this.state.topSubmission[2].total_comments+
                      this.state.topSubmission[3].total_comments+this.state.topSubmission[4].total_comments+this.state.topSubmission[5].total_comments+
                      this.state.topSubmission[6].total_comments+this.state.topSubmission[7].total_comments+this.state.topSubmission[8].total_comments+
                      this.state.topSubmission[9].total_comments}
                  </CardTitle>
                </CardHeader>
                <CardBody>
                  <div className="chart-area">
                    <Line
                      data={chartExample5.data}
                      options={chartExample5.options}
                    />
                  </div>
                </CardBody>
              </Card>
            </Col>
            <Col >
              <Card className="card-chart">
                <CardHeader>
                  <h5 className="card-category">Top Idiomas</h5>
                  <CardTitle tag="h3">
                    <i className="tim-icons icon-delivery-fast text-primary" />{" "}
                    {this.state.topLanguages[0].total_comments+this.state.topLanguages[1].total_comments+this.state.topLanguages[2].total_comments+
                      this.state.topLanguages[3].total_comments+this.state.topLanguages[4].total_comments+this.state.topLanguages[5].total_comments+
                      this.state.topLanguages[6].total_comments+this.state.topLanguages[7].total_comments+this.state.topLanguages[8].total_comments+
                      this.state.topLanguages[9].total_comments}
                  </CardTitle>
                </CardHeader>
                <CardBody>
                  <div className="chart-area">
                    <Bar
                      data={chartExample6.data}
                      options={chartExample6.options}
                    />
                  </div>
                </CardBody>
              </Card>
            </Col>

          </Row>
          

        </div>
      </>
    );
  }
}

export default Dashboard;
