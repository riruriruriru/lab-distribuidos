from peewee import *

psql_db = PostgresqlDatabase('db', user='riruriru', password='riruriru',
                           host='35.239.147.51', port=4040)#datos de conexion de bd
print(psql_db)
print(psql_db.connect())
class BaseModel(Model):
    
    class Meta:#modelo para la conexion y otras cosas generales
        database = psql_db

class User(BaseModel):#las entidades que se van a manejar en la bd se pueden crear como clases
    username = CharField()#pueden tener fields
#psql_db.create_tables([User])
#psql_db.drop_all()
models = [User]
def recreate_tables(models):#se pueden dropear las tablas o crear 
    psql_db.drop_tables(models)
    psql_db.create_tables(models)
recreate_tables(models)
User.create(username='Charlie')#insertar elementos
User.create(username='Doviarab')
print(User.get(User.username == 'Charlie').username)
print(User.get(User.username == 'Doviarab').username)
print("uwu")
query = User.select()#se hace un select de todos los user
[print(user.username) for user in query]#se muestran sus username
