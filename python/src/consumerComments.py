from kafka import KafkaConsumer
from json import loads
from textblob import TextBlob
import pycountry
import nltk
from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize 
from vaderSentiment.vaderSentiment import SentimentIntensityAnalyzer

import pymongo
from pymongo import MongoClient
from pymongo import *

vowels = "aeiouAEIOU"
consonants = "bcdfghjklmnñpqrstvwxyzBCDFGHJKLMNÑPQRSTVWXYZ"
analyser = SentimentIntensityAnalyzer()
consumer = KafkaConsumer('comentarios',
						 bootstrap_servers=['35.239.147.51:9092'], 
						 auto_offset_reset='earliest', 
						 enable_auto_commit=True, 
						 value_deserializer=lambda x: loads(x.decode('utf-8')))


def countVowelsandConsonants(string):
	print(string)
	num_vowels=0
	num_consonant=0
	num_upper = 0
	num_lower = 0
	for char in string:
		if char.isupper():
			num_upper = num_upper+1
		elif char.islower():
			num_lower = num_lower+1
		if char in vowels:
		   num_vowels = num_vowels+1
		elif char in consonants:
			num_consonant = num_consonant+1 
	return num_vowels, num_consonant, num_upper, num_lower
	
def sentiment_analyzer_scores(sentence):
    score = analyser.polarity_scores(sentence)
    print("{:-<40} {}".format(sentence, str(score)))
    return score
    	
def countStopWords(text):
	try:
		lan = text.detect_language()
	except:
		print("no se puede detectar idioma ni filtrar stopwords, muy corto")
		return [], 0, [], 0, " "
	language = pycountry.languages.get(alpha_2=lan)
	lanName = TextBlob(language.name).lower()
	#print("lanName: ", lanName)
	try:
		stop_words = set(stopwords.words(str(lanName))) 
	except:
		#print("no hay stopwords para este idioma")
		return [], 0, [], 0, lanName
	filtered_sentence = [w for w in text.words if not w in stop_words]
	stop_words_in_sentence = [w for w in text.words if w in stop_words]
	return filtered_sentence, len(filtered_sentence), stop_words_in_sentence, len(stop_words_in_sentence), lanName  


client = MongoClient('35.239.147.51', username='riruriru', password='riruriru', authSource='db', authMechanism='SCRAM-SHA-1')#datos de conexion de bd
db = client["db"]#asi se accede a una bd
collection = db["reddit_comments"]#asi se accede a una coleccion


for message in consumer:
	messageBody = message.value['comment']
	print(message)
	#messageSubreddit = message.value['subreddit']
	text = TextBlob(messageBody)
	words_sin_stopwords= []
	words_stopwords = []
	languageName = ""
	numWordsSinStopwords = 0
	numStopWords = 0
	numVowels = 0
	numConsonants = 0
	numUpper = 0
	numLower = 0
	#print(countStopWords(text))
	#procesar y guardar datos
	words_sin_stopwords, numWordsSinStopwords, word_stopwords, numStopWords, languageName = countStopWords(text)
	numVowels, numConsonants, numUpper, numLower = countVowelsandConsonants(text)
	score = sentiment_analyzer_scores(messageBody)
	print("Total words: ", len(text.words))
	print("Total sentences: ", len(text.sentences))
	comment = {"Comentario": messageBody}
	print("Fin comentario...")
	insertMongo = {"id": message.value['id'], 
					   "author": message.value['author'], 
					   "body": message.value['comment'],
					   "score": message.value['score'],
					   "total_words": len(text.words),
					   "total_sentences": len(text.sentences),
					   "num_vowels": numVowels,
					   "num_consonants": numConsonants,
					   "num_upper": numUpper,
					   "num_lower": numLower,
					   "words_sin_stopwords": words_sin_stopwords,
					   "num_words_sin_stopwords": numWordsSinStopwords,
					   "word_stopwords": word_stopwords,
					   "num_stopwords": numStopWords,
					   "language": str(languageName),
					   "subreddit": message.value['subreddit'],
					   "post": message.value['submission'],
					   "neg": score["neg"],
					   "neu": score["neu"],
					   "pos": score["pos"],
					   "compound": score["compound"]
					   
					   }

	collection.insert_one(insertMongo)






