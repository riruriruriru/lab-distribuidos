
import praw
from praw.models import MoreComments
from vaderSentiment.vaderSentiment import SentimentIntensityAnalyzer
from textblob import TextBlob
import nltk
from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize 
import pycountry
import pymongo
from pymongo import MongoClient
from pymongo import *

#detectar lenguaje, setear stopwords y filtrar stopwords vs no stopwords
example_sent = "This is a sample sentence, showing off the stop words filtration."
print(TextBlob(example_sent).detect_language())
lan=TextBlob(example_sent).detect_language()
language = pycountry.languages.get(alpha_2=lan)
lanName = TextBlob(language.name).lower()
print(lanName)
stop_words = set(stopwords.words(str(lanName))) 
filtered_sentence = [w for w in TextBlob(example_sent).words if not w in stop_words] 
stop_words_in_sentence = [w for w in TextBlob(example_sent).words if w in stop_words] 
print(filtered_sentence)
print(stop_words_in_sentence)
def countStopWords(text):
	lan = text.detect_language()
	language = pycountry.languages.get(alpha_2=lan)
	lanName = TextBlob(language.name).lower()
	#print("lanName: ", lanName)
	try:
		stop_words = set(stopwords.words(str(lanName))) 
	except:
		#print("no hay stopwords para este idioma")
		return [], 0, [], 0, lanName
	filtered_sentence = [w for w in text.words if not w in stop_words]
	stop_words_in_sentence = [w for w in text.words if w in stop_words]
	return filtered_sentence, len(filtered_sentence), stop_words_in_sentence, len(stop_words_in_sentence), lanName  
	
	
vowels = "aeiouAEIOU"
consonants = "bcdfghjklmnñpqrstvwxyzBCDFGHJKLMNÑPQRSTVWXYZ"
#funcion que cuenta vocales
def countVowelsandConsonants(string):
	print(string)
	num_vowels=0
	num_consonant=0
	num_upper = 0
	num_lower = 0
	for char in string:
		if char.isupper():
			num_upper = num_upper+1
		elif char.islower():
			num_lower = num_lower+1
		if char in vowels:
		   num_vowels = num_vowels+1
		elif char in consonants:
			num_consonant = num_consonant+1 
	return num_vowels, num_consonant, num_upper, num_lower

print(countVowelsandConsonants(TextBlob(example_sent)))
print("Capital Letters: ", sum(1 for c in "holaa AaaaAA" if c.isupper()))
#exit()

analyser = SentimentIntensityAnalyzer()
#analisis de sentimiento simple con vader
def sentiment_analyzer_scores(sentence):
    score = analyser.polarity_scores(sentence)
    print(str(score))
    return score

#reddit
reddit = praw.Reddit(client_id='60oidpnpPNz_uQ', client_secret='2uPz_036SrFrOWS8aUGW01tTB6Q', user_agent='app-lab-distribuidos')
comments = reddit.redditor("doviarab").comments.new(limit=None)#se pueden obtener los comentarios de un usuario en especifico
print("reddit")
cont = 0
'''for comment in comments:
	print("///")
	print(comment.body)
	print("///")
	cont = cont +1
print(cont)
exit()'''
contadorFiltrado = 0
hot_posts = reddit.subreddit('all').hot(limit=1000)

#stream de submission en tiempo real, lo malo es que al ser nuevas no tienen comentarios
#o tienen comentarios recien hechos casi sin score
'''for submission in reddit.subreddit('all').stream.submissions():
	print("selftext submission: ", submission.selftext)
	if submission.over_18 == True:
		print("filtrado")
		contadorFiltrado = contadorFiltrado+1
	else:
		submission = reddit.submission(id=submission.id)
		for top_level_comment in submission.comments:
			if isinstance(top_level_comment, MoreComments):
				continue
			print(top_level_comment.body)
			print("score: ",top_level_comment.score)
'''		

#base de datos:
client = MongoClient('35.239.147.51', username='riruriru', password='riruriru', authSource='db', authMechanism='SCRAM-SHA-1')#datos de conexion de bd
#client = MongoClient('mongodb://riruriru:riruriru@35.239.147.51:27017/?authSource=admin&readPreference=primary&ssl=false')
db = client["db"]#asi se accede a una bd
collection = db["comments"]#asi se accede a una coleccion
print(db.collection_names())
collection.drop()
for comment in reddit.subreddit('all').stream.comments():
	#print(comment.submission)
	submission = reddit.submission(id=comment.submission)
	a = []
	b = []
	c = 0
	d = 0
	if submission.over_18 == True:
		print("filtrado...")
	else:
		print("comentario...")
		print("user: ",comment.author)
		print("subreddit: ",comment.subreddit)
		print("submission title: ", submission.title)
		text = TextBlob(comment.body)
		print(text.words)
		print(text.words.lemmatize())
		
		a, c, b, d, languageName = countStopWords(text)
		#print(a, c, b, d, languageName)
		score = sentiment_analyzer_scores(comment.body)
		print(score["neg"])
		print("stopwords...")
		print("total words: ",len(text.words))
		print("total sentences: ",len(text.sentences))
		comment = {"comentario": comment.body}#se puede ingresar un documento en una coleccion, debe tener formato json
		print("fin comentario...")
		post_id = collection.insert_one(comment).inserted_id
			
'''for post in hot_posts:
	#print(post.title)
	submission = reddit.submission(id=post.id)
	print("submission over 18:", submission.over_18)
	if submission.over_18 == True:
		print("filtrado uwu")
	else:		
		for top_level_comment in submission.comments:
			if isinstance(top_level_comment, MoreComments):
				continue
			#print(top_level_comment.author)
			print("##")
			sentiment_analyzer_scores(top_level_comment.body)
			text = TextBlob(top_level_comment.body)
			#contador de palabras en comentario
			print("CONTADOR PALABRAS: ",len(text.words))
			print("CONTADOR ORACIONES: ",len(text.sentences))
			print(text)
			print("PALABRAS: ", text.words)
			print("##")
			for palabra in text.words:
				print("contador palabras "+palabra+": "+str(text.words.count(palabra))) '''
			
